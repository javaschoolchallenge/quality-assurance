package com.tsystems.javaschool.challenge.qualityassurance;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class QualityAssuranceApplicationTests {

	@Test
	void contextLoads() {
	}

}
