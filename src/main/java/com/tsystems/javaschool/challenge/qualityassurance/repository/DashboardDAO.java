package com.tsystems.javaschool.challenge.qualityassurance.repository;

import com.tsystems.javaschool.challenge.qualityassurance.domain.Story;

import java.util.List;

public interface DashboardDAO {

    boolean getStarted();
    List<Story> getAllStories();
    void update(Story story);
    Long getLastTesterID();
}
