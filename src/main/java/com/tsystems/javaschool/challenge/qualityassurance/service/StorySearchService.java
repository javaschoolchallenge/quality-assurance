package com.tsystems.javaschool.challenge.qualityassurance.service;

public interface StorySearchService {

    void checkAvailableStories();

}
