package com.tsystems.javaschool.challenge.qualityassurance.service.impl;

import com.tsystems.javaschool.challenge.qualityassurance.domain.Employee;
import com.tsystems.javaschool.challenge.qualityassurance.domain.Story;
import com.tsystems.javaschool.challenge.qualityassurance.repository.DashboardDAO;
import com.tsystems.javaschool.challenge.qualityassurance.service.StorySearchService;
import com.tsystems.javaschool.challenge.qualityassurance.service.WorkService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@RequiredArgsConstructor
@Slf4j
public class StorySearchServiceImpl implements StorySearchService {

    private final DashboardDAO dashboardDAO;
    private final WorkService workService;
    private Employee tester;

    @PostConstruct
    public void initBot() {
        try {
            Long id = dashboardDAO.getLastTesterID();
            tester = new Employee("Test" + id, Employee.Position.TESTER);
        } catch (Exception e) {
            tester = new Employee("Test" + 1L, Employee.Position.TESTER);
        }
        log.info("Tester {} was initialized.", tester.getName());
    }

    @Override
    @Scheduled(fixedDelay = 1000)
    public void checkAvailableStories() {
        if (!dashboardDAO.getStarted()) {
            log.info("Sprint is not started, waiting...");
            return;
        }
        List<Story> openStories = dashboardDAO.getAllStories()
                .stream()
                .filter(story -> story.getStatus() == Story.Status.RESOLVED)
                .sorted(Comparator.comparingInt(Story::getPriority))
                .collect(toList());
        log.info("Found {} open stories", openStories.size());
        if (!CollectionUtils.isEmpty(openStories)) {
            workService.takeStoryForWork(openStories.get(0), tester);
        } else {
            log.info("There are no open stories.");
        }
    }

}
