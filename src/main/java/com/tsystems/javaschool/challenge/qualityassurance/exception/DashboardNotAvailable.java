package com.tsystems.javaschool.challenge.qualityassurance.exception;

public class DashboardNotAvailable extends RuntimeException {

    public DashboardNotAvailable() {
    }

    public DashboardNotAvailable(String message) {
        super(message);
    }

    public DashboardNotAvailable(String message, Throwable cause) {
        super(message, cause);
    }

    public DashboardNotAvailable(Throwable cause) {
        super(cause);
    }

}
