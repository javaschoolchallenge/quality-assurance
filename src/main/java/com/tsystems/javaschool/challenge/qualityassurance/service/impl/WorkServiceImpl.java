package com.tsystems.javaschool.challenge.qualityassurance.service.impl;

import com.tsystems.javaschool.challenge.qualityassurance.domain.Employee;
import com.tsystems.javaschool.challenge.qualityassurance.domain.Story;
import com.tsystems.javaschool.challenge.qualityassurance.repository.DashboardDAO;
import com.tsystems.javaschool.challenge.qualityassurance.service.WorkService;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class WorkServiceImpl implements WorkService {

    @Value("${business.sp-multiplier}")
    private long spMultiplier;

    private final DashboardDAO dashboardDAO;

    @Override
    @SneakyThrows
    public void takeStoryForWork(Story story, Employee tester) {
        story.setStatus(Story.Status.IN_TEST);
        story.setAssigned(tester);
        dashboardDAO.update(story);
        log.info("Took story for work: {}", story);
        log.info("Estimated work time: {}", spMultiplier * story.getEstimation());
        Thread.sleep(spMultiplier * story.getEstimation());
        story.setStatus(Story.Status.CLOSED);
        story.setAssigned(null);
        dashboardDAO.update(story);
        log.info("Story {} is closed!", story.getId());
    }

}
