package com.tsystems.javaschool.challenge.qualityassurance.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    private String name;
    private Position position;

    public enum  Position {
        TESTER, DEVELOPER, ANALYST
    }
}
