package com.tsystems.javaschool.challenge.qualityassurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QualityAssuranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(QualityAssuranceApplication.class, args);
	}

}
