package com.tsystems.javaschool.challenge.qualityassurance.service;

import com.tsystems.javaschool.challenge.qualityassurance.domain.Employee;
import com.tsystems.javaschool.challenge.qualityassurance.domain.Story;

public interface WorkService {

    void takeStoryForWork(Story story, Employee tester);

}
